# count_words_in_string.py
# Programmer: Demar Brown (Demar Codes)
# Date: Jan 1, 2021
# Program Details: This program counts the number of individual words in a string.
                    
#-------------------
# ------------------------------
def word_count(user_string):

    word_cnt= 0

    user_input = user_string.split(" ")

    for word in user_input:
        word_cnt +=1

    return word_cnt

#Program starts here

user_string =  input("\nThis is a Word Counter. Enter your string here: ")
print("The number  of words found in the string is: " + str(word_count(user_string)))